package br.com.yahtosistemas.yapont.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import br.com.yahtosistemas.yapont.security.AppUserDetailsService;

/**
 * 
 * @author Yahto Sistemas - Felipe Corrêa
 * 
 */
@EnableWebSecurity
@ComponentScan(basePackageClasses = {AppUserDetailsService.class})
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsService userDetailsService;
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
	}
	
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring()
			.antMatchers("/layout/**")
			.antMatchers("/images/**");
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.authorizeRequests()
			.antMatchers("/usuarios/novo").hasRole("ADMINISTRADOR")
				.antMatchers("/usuarios/*").hasAnyRole("ADMINISTRADOR", "OPERACIONAL")
				.antMatchers("/usuarios").hasAnyRole("ADMINISTRADOR", "OPERACIONAL")
				.antMatchers("/projetos/*").hasRole("ADMINISTRADOR")
				.antMatchers("/projetos").hasAnyRole("ADMINISTRADOR", "OPERACIONAL")
				.antMatchers("/apontamentos/novo").hasRole("OPERACIONAL")
				.antMatchers("/apontamentos").hasAnyRole("ADMINISTRADOR", "OPERACIONAL")
				.anyRequest().authenticated()
				.and()
			.formLogin()
				.loginPage("/login")
				.permitAll()
				.and()
			.logout()
				.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
				.and()
			.rememberMe()
				.key("yahtoYapont")
				.tokenValiditySeconds(2419200) // 1 semana 604800, esta configurado para 4 semanas
				.userDetailsService(userDetailsService)
				.and()
			.sessionManagement()
				.invalidSessionUrl("/login");
	}
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

}
