package br.com.yahtosistemas.yapont.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.yahtosistemas.yapont.controller.page.PageWrapper;
import br.com.yahtosistemas.yapont.model.Apontamento;
import br.com.yahtosistemas.yapont.repository.Apontamentos;
import br.com.yahtosistemas.yapont.repository.filter.ApontamentoFilter;
import br.com.yahtosistemas.yapont.security.UsuarioSistema;
import br.com.yahtosistemas.yapont.service.CadastroApontamentoService;
import br.com.yahtosistemas.yapont.service.exception.HorarioDeInicioDeveSerInferiorAoHorarioFimException;

@Controller
@RequestMapping("/apontamentos")
public class ApontamentosController {
	
	@Autowired
	private Apontamentos apontamentos;
	
	@Autowired
	private CadastroApontamentoService cadastroApontamentoService;

	@GetMapping
	public ModelAndView pesquisar(ApontamentoFilter apontamentoFilter, HttpServletRequest request, 
			@AuthenticationPrincipal UsuarioSistema usuarioSistema, @PageableDefault(size = 10) Pageable pageable, HttpServletRequest httpServletRequest) {
		
		ModelAndView mv = new ModelAndView("apontamento/PesquisaApontamentos");
			
		if (request.isUserInRole("ADMINISTRADOR")) {
			apontamentoFilter.setAdminLogado(true);
		} else {
			apontamentoFilter.setUsuario(usuarioSistema.getUsuario());
		}
		PageWrapper<Apontamento> paginaWrapper = new PageWrapper<>(apontamentos.filtrar(apontamentoFilter, pageable)
				, httpServletRequest);
		mv.addObject("pagina", paginaWrapper);
		return mv;
	}

	@GetMapping("/novo")
	public ModelAndView novo(Apontamento apontamento) {
		ModelAndView mv = new ModelAndView("apontamento/CadastroApontamento");
		mv.addObject(apontamento);
		return mv;
	}
	
	@PostMapping("/novo")
	public ModelAndView salvar(@Valid Apontamento apontamento, BindingResult result, RedirectAttributes attributes,
			@AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		
		if (result.hasErrors()) {
			return novo(apontamento);
		}
		
		apontamento.setUsuario(usuarioSistema.getUsuario());
		
		try {			
			cadastroApontamentoService.salvar(apontamento);
		} catch (HorarioDeInicioDeveSerInferiorAoHorarioFimException e) {
			result.rejectValue("horarioFim", e.getMessage(), e.getMessage());
			return novo(apontamento);
		}
		
		attributes.addFlashAttribute("mensagem", "Apontamento salvo com sucesso");
		return new ModelAndView("redirect:/apontamentos/novo");
	}
	
	@GetMapping(path = "/{codigoProjeto}", consumes = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody Apontamento buscarApontamentoParaRegistrar(@PathVariable Long codigoProjeto, @AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		Apontamento apontamento = apontamentos.buscarParaRegistrarPorProjetoEPorUsuarioLogado(codigoProjeto, usuarioSistema.getUsuario().getCodigo()); 
		if (apontamento != null) {
			apontamento.setUsuario(null);
		}
		return apontamento != null ? apontamento : new Apontamento();
	}
}
