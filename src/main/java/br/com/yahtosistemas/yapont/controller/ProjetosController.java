package br.com.yahtosistemas.yapont.controller;

import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.yahtosistemas.yapont.controller.page.PageWrapper;
import br.com.yahtosistemas.yapont.dto.ProjetoDTO;
import br.com.yahtosistemas.yapont.model.Projeto;
import br.com.yahtosistemas.yapont.model.StatusProjeto;
import br.com.yahtosistemas.yapont.model.Usuario;
import br.com.yahtosistemas.yapont.repository.Projetos;
import br.com.yahtosistemas.yapont.repository.Usuarios;
import br.com.yahtosistemas.yapont.repository.filter.ProjetoFilter;
import br.com.yahtosistemas.yapont.security.UsuarioSistema;
import br.com.yahtosistemas.yapont.service.CadastroProjetoService;
import br.com.yahtosistemas.yapont.service.exception.ImpossivelExcluirEntidadeException;
import br.com.yahtosistemas.yapont.service.exception.UsuarioObrigatorioParaProjetoException;
import br.com.yahtosistemas.yapont.session.TabelaUsuariosProjetoSession;

@Controller
@RequestMapping("/projetos")
public class ProjetosController {

	@Autowired
	private CadastroProjetoService cadastroProjetoService;
	
	@Autowired
	private TabelaUsuariosProjetoSession tabelaUsuariosProjeto;
	
	@Autowired
	private Projetos projetos;
	
	@Autowired
	private Usuarios usuarios;
	
	
	@GetMapping
	public ModelAndView pesquisar(ProjetoFilter projetoFilter, @AuthenticationPrincipal UsuarioSistema usuarioSistema
			, @PageableDefault(size = 10) Pageable pageable, HttpServletRequest httpServletRequest) {
		
		ModelAndView mv = new ModelAndView("projeto/PesquisaProjetos");
			
		projetoFilter.setUsuario(usuarioSistema.getUsuario());
		PageWrapper<Projeto> paginaWrapper = new PageWrapper<>(projetos.filtrar(projetoFilter, pageable)
				, httpServletRequest);
		mv.addObject("pagina", paginaWrapper);
		return mv;
	}

	@GetMapping("/novo")
	public ModelAndView novo(Projeto projeto) {
		setUuid(projeto);
		
		ModelAndView mv = new ModelAndView("projeto/CadastroProjeto");
		mv.addObject("osStatusProjeto", StatusProjeto.values());
		mv.addObject("usuarios", projeto.getUsuarios());
		mv.addObject(projeto);
		return mv;
	}
	
	@GetMapping("/{codigo}")
	public ModelAndView editar(@PathVariable Long codigo) {
		Projeto projeto = projetos.buscarComUsuarios(codigo);
		
		if (projeto != null) {
			if (projeto.getStatusProjeto().equals(StatusProjeto.FINALIZADO) || 
					projeto.getStatusProjeto().equals(StatusProjeto.CANCELADO)) {
				projeto.setDetalhes(true);
			}
			setUuid(projeto);
			projeto.getUsuarios().forEach((u )-> {
				u.setDetalhes(projeto.isDetalhes());
				tabelaUsuariosProjeto.adicionarUsuario(projeto.getUuid(), u);
			});
			
			return novo(projeto);
		}
		
		return new ModelAndView("404");
	}
	
	@PostMapping({ "/novo", "{\\+d}" })
	public ModelAndView salvar(@Valid Projeto projeto, BindingResult result, RedirectAttributes attributes) {
		
		projeto.setUsuarios(tabelaUsuariosProjeto.getUsuarios(projeto.getUuid()));
		if (result.hasErrors()) {
			return novo(projeto);
		}
		
		try {			
			cadastroProjetoService.salvar(projeto);
		} catch (UsuarioObrigatorioParaProjetoException e) {
			result.rejectValue("usuarios", e.getMessage(), e.getMessage());
			return novo(projeto);
		}
		
		attributes.addFlashAttribute("mensagem", "Projeto salvo com sucesso");
		return new ModelAndView("redirect:/projetos/novo");
	}
	
	@DeleteMapping("/{codigo}")
	public @ResponseBody ResponseEntity<?> excluir(@PathVariable("codigo") Projeto projeto) {
		try {
			cadastroProjetoService.excluir(projeto);
		} catch (ImpossivelExcluirEntidadeException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
		return ResponseEntity.ok().build();
	}
	
	@PostMapping("/usuario")
	public ModelAndView adicionarUsuario(Long codigoUsuario, String uuid) {
		Usuario usuario = usuarios.getOne(codigoUsuario);
		tabelaUsuariosProjeto.adicionarUsuario(uuid, new Usuario(usuario.getCodigo(), usuario.getCpf(), usuario.getNome()));
		return mvTabelaUsuariosProjeto(uuid);
	}
	
	@DeleteMapping("/usuario/{uuid}/{codigoUsuario}")
	public ModelAndView excluirAtividade(@PathVariable("codigoUsuario") Usuario usuario, @PathVariable("uuid") String uuid){
		tabelaUsuariosProjeto.excluirUsuario(uuid, usuario);
		return mvTabelaUsuariosProjeto(uuid);
	}
	
	@RequestMapping(consumes = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody List<ProjetoDTO> pesquisar(@AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		return projetos.buscarProjetosEmAndamentoEPorUsuarioLogado(usuarioSistema.getUsuario().getCodigo());
	}

	private ModelAndView mvTabelaUsuariosProjeto(String uuid) {
		ModelAndView mv = new ModelAndView("projeto/TabelaUsuariosProjeto");
		mv.addObject("usuarios", tabelaUsuariosProjeto.getUsuarios(uuid));
		return mv;
	}

	private void setUuid(Projeto projeto) {
		if (StringUtils.isEmpty(projeto.getUuid())) {
			projeto.setUuid(UUID.randomUUID().toString());
		}
	}
	
	@ExceptionHandler(IllegalArgumentException.class)
	public ResponseEntity<Void> tratarIllegalArgumentException(IllegalArgumentException e) {
		return ResponseEntity.badRequest().build();
	}
}
