package br.com.yahtosistemas.yapont.dto;

public class UsuarioDTO {

	private Long codigo;
	private String cpf;
	private String nome;
	
	public UsuarioDTO(Long codigo, String cpf, String nome) {
		this.codigo = codigo;
		this.cpf = cpf;
		this.nome = nome;
	}
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
