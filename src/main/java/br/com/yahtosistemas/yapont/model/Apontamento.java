package br.com.yahtosistemas.yapont.model;

import java.io.Serializable;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostLoad;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "apontamento")
public class Apontamento implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long codigo;
	
	@Column(name = "tenant_id")
	private String tenantId;
	
	@ManyToOne
	@JoinColumn(name = "codigo_usuario")
	private Usuario usuario;
	
	@ManyToOne
	@JoinColumn(name = "codigo_projeto")
	private Projeto projeto;
	
	@Column(name = "data_hora_entrada")
	private LocalDateTime dataHoraEntrada;
	
	@Column(name = "data_hora_saida")
	private LocalDateTime dataHoraSaida;
	
	private String observacao;

	@NotNull
	@Transient
	private LocalDate dataApontamento = LocalDate.now();

	@NotNull(message = "Horário de inicio é obrigatório")
	@Transient
	private LocalTime horarioInicio;
	
	@Transient
	private LocalTime horarioFim;
	
	@Transient
	private boolean detalhes = false;
	
	@PostLoad
	private void postLoad() {
		if (this.dataHoraEntrada != null) {
			this.horarioInicio = this.dataHoraEntrada.toLocalTime();
		}
		if (this.dataHoraSaida != null) {
			this.horarioFim = this.dataHoraSaida.toLocalTime();
		}
	}

	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Projeto getProjeto() {
		return projeto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	public LocalDateTime getDataHoraEntrada() {
		return dataHoraEntrada;
	}
	public void setDataHoraEntrada(LocalDateTime dataHoraEntrada) {
		this.dataHoraEntrada = dataHoraEntrada;
	}

	public LocalDateTime getDataHoraSaida() {
		return dataHoraSaida;
	}
	public void setDataHoraSaida(LocalDateTime dataHoraSaida) {
		this.dataHoraSaida = dataHoraSaida;
	}

	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	public LocalDate getDataApontamento() {
		return dataApontamento;
	}
	public void setDataApontamento(LocalDate dataApontamento) {
		this.dataApontamento = dataApontamento;
	}
	
	public LocalTime getHorarioInicio() {
		return horarioInicio;
	}
	public void setHorarioInicio(LocalTime horarioInicio) {
		this.horarioInicio = horarioInicio;
	}
	
	public LocalTime getHorarioFim() {
		return horarioFim;
	}
	public void setHorarioFim(LocalTime horarioFim) {
		this.horarioFim = horarioFim;
	}
	
	public boolean isDetalhes() {
		return detalhes;
	}
	public void setDetalhes(boolean detalhes) {
		this.detalhes = detalhes;
	}
	
	public boolean isNovo() {
		return this.codigo == null;
	}
	
	public String getDiaDaSemanaApontamento() {
		return this.dataApontamento.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + getDiaDaSemanaEmPortuges(this.dataApontamento.getDayOfWeek());
	}
	
	private String getDiaDaSemanaEmPortuges(DayOfWeek dayOfWeek) {
		switch (dayOfWeek) {
		case MONDAY:
			return " - Segunda-feira";
		case TUESDAY:
			return " - Terça-feira";
		case WEDNESDAY:
			return " - Quarta-feira";
		case THURSDAY:
			return " - Quinta-feira";
		case FRIDAY:
			return " - Sexta-feira";
		case SATURDAY:
			return " - Sabado";
		case SUNDAY:
			return " - Domingo";
		default:
			return null;
		}
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Apontamento other = (Apontamento) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}
}
