package br.com.yahtosistemas.yapont.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "projeto")
public class Projeto implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long codigo;
	
	@Column(name = "tenant_id")
	private String tenantId;
	
	@NotBlank(message = "Informar a descrição")
	private String descricao;
	
	private String observacao;
	
	@Enumerated(EnumType.STRING)
	@NotNull(message = "Informar o status")
	@Column(name = "status")
	private StatusProjeto statusProjeto;
	
	@Column(name = "data_cadastro")
	private LocalDate dataCadastro = LocalDate.now();
	
	@ManyToMany
	@JoinTable(name = "projeto_usuario", joinColumns = @JoinColumn(name = "codigo_projeto")
				, inverseJoinColumns = @JoinColumn(name = "codigo_usuario"))	
	private List<Usuario> usuarios;
	
	@Transient
	private String uuid;
	
	@Transient
	private boolean detalhes = false;

	public Projeto() {	}
	
	public Projeto(Long codigoProjeto) {
		this.codigo = codigoProjeto;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public StatusProjeto getStatusProjeto() {
		return statusProjeto;
	}

	public void setStatusProjeto(StatusProjeto statusProjeto) {
		this.statusProjeto = statusProjeto;
	}

	public LocalDate getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(LocalDate dataCadastro) {
		this.dataCadastro = dataCadastro;
	}
	
	public List<Usuario> getUsuarios() {
		return usuarios;
	}
	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}
	
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public boolean isDetalhes() {
		return detalhes;
	}
	public void setDetalhes(boolean detalhes) {
		this.detalhes = detalhes;
	}
	
	public boolean isNovo() {
		return this.codigo == null;
	}
	
	public String getTituloCadastro() {
		if (this.detalhes) {
			return String.format("Detalhes do projeto - %s", this.getDescricao());
		} else {
			return isNovo() ? "Cadastro de projeto" : String.format("Edição do projeto - %s", this.getDescricao());
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Projeto other = (Projeto) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}
}
