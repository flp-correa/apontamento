package br.com.yahtosistemas.yapont.model;

public enum StatusProjeto {

	EM_ANDAMENTO("Em andamento"),
	FINALIZADO("Finalizado"),
	CANCELADO("Cancelado");
	
	private String descricao;
	
	private StatusProjeto(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
}
