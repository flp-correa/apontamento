package br.com.yahtosistemas.yapont.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.yahtosistemas.yapont.model.Apontamento;
import br.com.yahtosistemas.yapont.repository.helper.apontamento.ApontamentosQueries;

@Repository
public interface Apontamentos extends JpaRepository<Apontamento, Long>, ApontamentosQueries {

}
