package br.com.yahtosistemas.yapont.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.yahtosistemas.yapont.model.Grupo;

public interface Grupos extends JpaRepository<Grupo, Long> {

}
