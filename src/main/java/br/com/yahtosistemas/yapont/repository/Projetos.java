package br.com.yahtosistemas.yapont.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.yahtosistemas.yapont.model.Projeto;
import br.com.yahtosistemas.yapont.repository.helper.projeto.ProjetosQueries;

@Repository
public interface Projetos extends JpaRepository<Projeto, Long>, ProjetosQueries {

	@Transactional(readOnly = true)
	public List<Projeto> findByDescricaoStartingWithIgnoreCase(String descricao);

}
