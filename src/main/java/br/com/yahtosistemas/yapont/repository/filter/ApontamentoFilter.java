package br.com.yahtosistemas.yapont.repository.filter;

import java.time.LocalDate;

import br.com.yahtosistemas.yapont.model.Projeto;
import br.com.yahtosistemas.yapont.model.Usuario;

public class ApontamentoFilter {

	private LocalDate dataInicio;
	private LocalDate dataFim;
	private Projeto projeto;
	private Usuario usuario;
	private boolean adminLogado;

	public LocalDate getDataInicio() {
		return dataInicio;
	}
	public void setDataInicio(LocalDate dataInicio) {
		this.dataInicio = dataInicio;
	}
	
	public LocalDate getDataFim() {
		return dataFim;
	}
	public void setDataFim(LocalDate dataFim) {
		this.dataFim = dataFim;
	}
	
	public Projeto getProjeto() {
		return projeto;
	}
	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public boolean isAdminLogado() {
		return adminLogado;
	}
	public void setAdminLogado(boolean adminLogado) {
		this.adminLogado = adminLogado;
	}
}
