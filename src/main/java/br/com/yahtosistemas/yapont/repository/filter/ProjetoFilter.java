package br.com.yahtosistemas.yapont.repository.filter;

import br.com.yahtosistemas.yapont.model.Usuario;

public class ProjetoFilter {

	private Long codigo;
	private String descricao;
	private Usuario usuario;
	
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
