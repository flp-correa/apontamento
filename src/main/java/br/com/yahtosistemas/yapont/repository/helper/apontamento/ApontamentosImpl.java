package br.com.yahtosistemas.yapont.repository.helper.apontamento;

import java.time.LocalDateTime;
import java.time.LocalTime;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import br.com.yahtosistemas.yapont.model.Apontamento;
import br.com.yahtosistemas.yapont.model.Usuario;
import br.com.yahtosistemas.yapont.repository.filter.ApontamentoFilter;
import br.com.yahtosistemas.yapont.repository.paginacao.PaginacaoUtil;

public class ApontamentosImpl implements ApontamentosQueries {

	@PersistenceContext
	private EntityManager manager;
	
	@Autowired
	private PaginacaoUtil paginacaoUtil;
	
	@Transactional(readOnly = true)
	@SuppressWarnings({ "deprecation", "unchecked" })
	@Override
	public Page<Apontamento> filtrar(ApontamentoFilter filtro, Pageable pageable) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Apontamento.class);
		
		paginacaoUtil.preparar(criteria, pageable);
		adicionarFiltro(filtro, criteria);
		
		return new PageImpl<>(criteria.list(), pageable, total(filtro));
	}
	
	@Transactional(readOnly = true)
	@SuppressWarnings("deprecation")
	@Override
	public Apontamento buscarParaRegistrarPorProjetoEPorUsuarioLogado(Long codigoProjeto, Long codigoUsuario) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Apontamento.class);
		criteria.createAlias("usuario", "u");
		criteria.createAlias("projeto", "p");
		criteria.add(Restrictions.eq("u.codigo", codigoUsuario));
		criteria.add(Restrictions.eq("p.codigo", codigoProjeto));
		criteria.add(Restrictions.isNull("dataHoraSaida"));
		return (Apontamento) criteria.uniqueResult();
	}
	
	@SuppressWarnings("deprecation")
	private Long total(ApontamentoFilter filtro) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Apontamento.class);
		adicionarFiltro(filtro, criteria);
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}
	
	private void adicionarFiltro(ApontamentoFilter filtro, Criteria criteria) {
		if (filtro != null) {
			if (filtro.getUsuario() != null) {			
				if (!filtro.isAdminLogado()) {
					criteria.add(Restrictions.eq("usuario", filtro.getUsuario()));
				} else if (!StringUtils.isEmpty(filtro.getUsuario().getNome())) {
					DetachedCriteria dc = DetachedCriteria.forClass(Usuario.class);
					dc.add(Restrictions.ilike("nome", filtro.getUsuario().getNome(), MatchMode.ANYWHERE));
					dc.setProjection(Projections.property("codigo"));
					
					criteria.add(Restrictions.and(Subqueries.propertyIn("usuario.codigo", dc)));
				}
			}
		
			if (filtro.getDataInicio() != null) {
				criteria.add(Restrictions.ge("dataHoraEntrada", LocalDateTime.of(filtro.getDataInicio(), LocalTime.MIDNIGHT)));
			} 
			if (filtro.getDataFim() != null) {
				criteria.add(Restrictions.le("dataHoraEntrada", LocalDateTime.of(filtro.getDataFim(), LocalTime.MAX)));
			}
		}
	}
}
