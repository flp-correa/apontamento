package br.com.yahtosistemas.yapont.repository.helper.apontamento;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.yahtosistemas.yapont.model.Apontamento;
import br.com.yahtosistemas.yapont.repository.filter.ApontamentoFilter;

public interface ApontamentosQueries {
	
	public Page<Apontamento> filtrar(ApontamentoFilter filtro, Pageable pageable);

	public Apontamento buscarParaRegistrarPorProjetoEPorUsuarioLogado(Long codigoProjeto, Long codigoUsuario);
}
