package br.com.yahtosistemas.yapont.repository.helper.projeto;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import br.com.yahtosistemas.yapont.dto.ProjetoDTO;
import br.com.yahtosistemas.yapont.model.Projeto;
import br.com.yahtosistemas.yapont.repository.filter.ProjetoFilter;
import br.com.yahtosistemas.yapont.repository.paginacao.PaginacaoUtil;

public class ProjetosImpl implements ProjetosQueries {

	@PersistenceContext
	private EntityManager manager;
	
	@Autowired
	private PaginacaoUtil paginacaoUtil;
	
	@Transactional(readOnly = true)
	@SuppressWarnings({ "deprecation", "unchecked" })
	@Override
	public Page<Projeto> filtrar(ProjetoFilter filtro, Pageable pageable) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Projeto.class);
		
		paginacaoUtil.preparar(criteria, pageable);
		adicionarFiltro(filtro, criteria);
		
		List<Projeto> filtrados = criteria.list();
		filtrados.forEach(u -> Hibernate.initialize(u.getUsuarios()));
		
		return new PageImpl<>(criteria.list(), pageable, total(filtro));
	}
	
	@Transactional(readOnly = true)
	@SuppressWarnings("deprecation")
	@Override
	public Projeto buscarComUsuarios(Long codigo) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Projeto.class);
		criteria.createAlias("usuarios", "u", JoinType.LEFT_OUTER_JOIN);
		criteria.add(Restrictions.eq("codigo", codigo));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (Projeto) criteria.uniqueResult();
	}
	
	@Transactional(readOnly = true)
	@Override
	public List<ProjetoDTO> buscarProjetosEmAndamentoEPorUsuarioLogado(Long codigoUsuario) {
		String jpql = "select new br.com.yahtosistemas.yapont.dto.ProjetoDTO(codigo, descricao) " 
				+ "from Projeto where statusProjeto = 'EM_ANDAMENTO' and exists "
				+ "(select 1 from ProjetoUsuario where id.projeto.codigo = codigo and id.usuario.codigo = :codigoUsuario)";
	
		List<ProjetoDTO> projetosFiltrados = manager.createQuery(jpql, ProjetoDTO.class)
			.setParameter("codigoUsuario", codigoUsuario)
			.getResultList();
		
		return projetosFiltrados;
	}
	
	private Long total(ProjetoFilter filtro) {
		@SuppressWarnings("deprecation")
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Projeto.class);
		adicionarFiltro(filtro, criteria);
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}
	
	private void adicionarFiltro(ProjetoFilter filtro, Criteria criteria) {
		if (filtro != null) {
			if (filtro.getCodigo() != null) {
				criteria.add(Restrictions.eq("codigo", filtro.getCodigo()));
			} else if (!StringUtils.isEmpty(filtro.getDescricao())) {
				criteria.add(Restrictions.ilike("descricao", filtro.getDescricao(), MatchMode.ANYWHERE));
			}
		}
	}
}
