package br.com.yahtosistemas.yapont.repository.helper.projeto;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.yahtosistemas.yapont.dto.ProjetoDTO;
import br.com.yahtosistemas.yapont.model.Projeto;
import br.com.yahtosistemas.yapont.repository.filter.ProjetoFilter;

public interface ProjetosQueries {

	public Page<Projeto> filtrar(ProjetoFilter filtro, Pageable pageable);
	
	public Projeto buscarComUsuarios(Long codigo);
	
	public List<ProjetoDTO> buscarProjetosEmAndamentoEPorUsuarioLogado(Long codigoUsuario);
}
