package br.com.yahtosistemas.yapont.service;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.yahtosistemas.yapont.model.Apontamento;
import br.com.yahtosistemas.yapont.repository.Apontamentos;
import br.com.yahtosistemas.yapont.service.exception.HorarioDeInicioDeveSerInferiorAoHorarioFimException;

@Service
public class CadastroApontamentoService {

	@Autowired
	private Apontamentos apontamentos;
	
	@Transactional
	public void salvar(Apontamento apontamento) {
		
		validarHorario(apontamento);
		
		apontamento.setDataHoraEntrada(LocalDateTime.of(apontamento.getDataApontamento(), apontamento.getHorarioInicio()));
		
		if (apontamento.getHorarioFim() != null) {
			apontamento.setDataHoraSaida(LocalDateTime.of(apontamento.getDataApontamento(), apontamento.getHorarioFim()));
		}
		
		apontamento.setTenantId("yahto-sistemas");
		apontamentos.save(apontamento);
	}

	private void validarHorario(Apontamento apontamento) {
		if (apontamento.getHorarioFim() != null && 
				(apontamento.getHorarioFim().isBefore(apontamento.getHorarioInicio()) || apontamento.getHorarioFim().equals(apontamento.getHorarioInicio()))) {
			throw new HorarioDeInicioDeveSerInferiorAoHorarioFimException("Horário de inicio deve ser inferior ao horário final");
		}
		
	}

	
}
