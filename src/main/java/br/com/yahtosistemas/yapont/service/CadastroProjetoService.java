package br.com.yahtosistemas.yapont.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.yahtosistemas.yapont.model.Projeto;
import br.com.yahtosistemas.yapont.repository.Projetos;
import br.com.yahtosistemas.yapont.service.exception.ImpossivelExcluirEntidadeException;
import br.com.yahtosistemas.yapont.service.exception.UsuarioObrigatorioParaProjetoException;

@Service
public class CadastroProjetoService {

	@Autowired
	private Projetos projetos;
	
	@Transactional
	public void salvar(Projeto projeto) {
		
		if (projeto.getUsuarios().isEmpty()) {
			throw new UsuarioObrigatorioParaProjetoException("Adicione pelo menos um usuário");
		}
		projeto.setTenantId("yahto-sistemas");
		projetos.save(projeto);
	}

	@Transactional
	public void excluir(Projeto projeto) {
		try {
			projetos.delete(projeto);
			projetos.flush();
		} catch (DataIntegrityViolationException e) {
			throw new ImpossivelExcluirEntidadeException("Impossível apagar projeto.");
		}		
	}

}
