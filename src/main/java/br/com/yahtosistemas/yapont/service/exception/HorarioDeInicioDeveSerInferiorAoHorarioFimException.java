package br.com.yahtosistemas.yapont.service.exception;

public class HorarioDeInicioDeveSerInferiorAoHorarioFimException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public HorarioDeInicioDeveSerInferiorAoHorarioFimException(String message) {
		super(message);
	}
}
