package br.com.yahtosistemas.yapont.service.exception;

public class UsuarioObrigatorioParaProjetoException extends RuntimeException {

private static final long serialVersionUID = 1L;
	
	public UsuarioObrigatorioParaProjetoException(String message) {
		super(message);
	}
}
