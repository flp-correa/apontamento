package br.com.yahtosistemas.yapont.session;

import java.util.ArrayList;
import java.util.List;

import br.com.yahtosistemas.yapont.model.Usuario;

class TabelaUsuariosProjeto {

	private String uuid;
	private List<Usuario> usuarios = new ArrayList<>();
	
	public TabelaUsuariosProjeto(String uuid) {
		this.uuid = uuid;
	}
	
	public void adicionarUsuario(Usuario usuario) {
		if (!usuarios.contains(usuario)) {
			usuarios.add(0, usuario);
		}
	}
	
	public void excluirUsuario(Usuario usuario) {
		usuarios.remove(usuario);
	}

	public int total(){
		return usuarios.size();
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}
	
	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public String getUuid() {
		return uuid;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TabelaUsuariosProjeto other = (TabelaUsuariosProjeto) obj;
		if (uuid == null) {
			if (other.uuid != null)
				return false;
		} else if (!uuid.equals(other.uuid))
			return false;
		return true;
	}
}
