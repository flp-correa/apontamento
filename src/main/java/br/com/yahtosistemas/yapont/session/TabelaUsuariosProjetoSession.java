package br.com.yahtosistemas.yapont.session;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import br.com.yahtosistemas.yapont.model.Usuario;

@SessionScope
@Component
public class TabelaUsuariosProjetoSession {

	private Set<TabelaUsuariosProjeto> tabelas = new HashSet<>();

	public void adicionarUsuario(String uuid, Usuario usuario) {
		TabelaUsuariosProjeto tabela = buscarTabelaPorUuid(uuid);
		
		tabela.adicionarUsuario(usuario);
		tabelas.add(tabela);
	}


	public void excluirUsuario(String uuid, Usuario usuario) {
		TabelaUsuariosProjeto tabela = buscarTabelaPorUuid(uuid);
		tabela.excluirUsuario(usuario);
	}

	public List<Usuario> getUsuarios(String uuid) {
		return buscarTabelaPorUuid(uuid).getUsuarios();
	}
	
	private TabelaUsuariosProjeto buscarTabelaPorUuid(String uuid) {
		TabelaUsuariosProjeto tabela = tabelas.stream()
				.filter(t -> t.getUuid().equals(uuid))
				.findAny()
				.orElse(new TabelaUsuariosProjeto(uuid));
		return tabela;
	}


	public void setUsuario(String uuid, List<Usuario> usuarios) {
		tabelas.stream()
				.filter(t -> t.getUuid().equals(uuid))
				.findAny()
				.get().setUsuarios(usuarios);
		
	}

}
