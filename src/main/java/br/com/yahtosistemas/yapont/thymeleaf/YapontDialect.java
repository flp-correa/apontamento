package br.com.yahtosistemas.yapont.thymeleaf;
import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Component;
import org.thymeleaf.dialect.AbstractProcessorDialect;
import org.thymeleaf.processor.IProcessor;
import org.thymeleaf.standard.StandardDialect;

import br.com.yahtosistemas.yapont.thymeleaf.processor.ClassForErrorAttributeTagProcessor;
import br.com.yahtosistemas.yapont.thymeleaf.processor.MenuAttributeTagProcessor;
import br.com.yahtosistemas.yapont.thymeleaf.processor.MessageElementTagProcessor;
import br.com.yahtosistemas.yapont.thymeleaf.processor.OrderElementTagProcessor;
import br.com.yahtosistemas.yapont.thymeleaf.processor.PaginationElementTagProcessor;



@Component
public class YapontDialect extends AbstractProcessorDialect {

	public YapontDialect() {
		super("Yahto Sistemas yapont", "yapont", StandardDialect.PROCESSOR_PRECEDENCE);
	}

	@Override
	public Set<IProcessor> getProcessors(String dialectPrefix) {
		final Set<IProcessor> processadores = new HashSet<>();
		processadores.add(new ClassForErrorAttributeTagProcessor(dialectPrefix));
		processadores.add(new MessageElementTagProcessor(dialectPrefix));
		processadores.add(new OrderElementTagProcessor(dialectPrefix));
		processadores.add(new PaginationElementTagProcessor(dialectPrefix));
		processadores.add(new MenuAttributeTagProcessor(dialectPrefix));
		return processadores;
	}

}
