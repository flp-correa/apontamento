CREATE TABLE projeto (
    codigo BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    tenant_id VARCHAR(50) NOT NULL,
    descricao VARCHAR(200) NOT NULL,
    observacao VARCHAR(500),
    status VARCHAR(50),
    data_cadastro DATE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE apontamento (
    codigo BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    tenant_id VARCHAR(50) NOT NULL,
    codigo_usuario BIGINT(20),
    codigo_projeto BIGINT(20),
    data_hora_entrada DATETIME,
    data_hora_saida DATETIME,
    observacao VARCHAR(500),
    FOREIGN KEY (codigo_usuario) REFERENCES usuario(codigo),
    FOREIGN KEY (codigo_projeto) REFERENCES projeto(codigo)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
