CREATE TABLE projeto_usuario (
    codigo_projeto BIGINT(20) NOT NULL,
    codigo_usuario BIGINT(20) NOT NULL,
    PRIMARY KEY (codigo_projeto, codigo_usuario),
    FOREIGN KEY (codigo_projeto) REFERENCES projeto(codigo),
    FOREIGN KEY (codigo_usuario) REFERENCES usuario(codigo)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
