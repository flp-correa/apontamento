Yapont.Apontamento = (function() {
	
	function Apontamento(codigoProjeto) {
		this.codigoProjeto = codigoProjeto;
		this.diaDaSemanaApontamento = $('.js-dia-da-semana-apontamento');
		this.diaApontamento = $('.js-dia-apontamento');
		this.horarioInicio = $('#horarioInicio');
		this.observacao = $('#observacao');
	}
	
	Apontamento.prototype.iniciar = function() {
		buscarDadosApontamento.call(this);
	}
	
	function buscarDadosApontamento() {		
		$.ajax({
			url: '/apontamentos/' + this.codigoProjeto,
			method: 'GET',
			contentType: 'application/json',
			success: onMostrarDadosApontamento.bind(this)
		});
	}
	
	function onMostrarDadosApontamento(apontamento) {
		this.diaApontamento.removeClass('hidden');
		
		formatarDados(apontamento);
		
		$('#codigo').val(apontamento.codigo);
		$('.js-dia-da-semana-apontamento').val(apontamento.diaDaSemanaApontamento);
		$('#horarioInicio').val(apontamento.horarioInicio);
		$('#horarioFim').val(apontamento.horarioFim);
		$('#observacao').val(apontamento.observacao);
		$('#dataApontamento').val(apontamento.dataApontamento);
	}
	
	function formatarDados(apontamento) {
		var dataApontamento = apontamento.dataApontamento;
		apontamento.dataApontamento = dataApontamento.trim().substr(8, 2) + '/' + dataApontamento.trim().substr(5, 2) + '/' + dataApontamento.trim().substr(0, 4);

		apontamento.horarioInicio = formatarHora(apontamento.horarioInicio);
		apontamento.horarioFim = formatarHora(apontamento.horarioFim);
	}
	
	function formatarHora(hora) {
		if (hora != null) {
			return hora.substr(0, 5);
		} else {
			return '';
		}
	}
	
	return Apontamento;
	
}());