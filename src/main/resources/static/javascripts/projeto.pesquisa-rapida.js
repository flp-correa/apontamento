Yapont = Yapont || {};

Yapont.PesquisaRapidaProjeto = (function() {
	
	function PesquisaRapidaProjeto() {
		this.pesquisaRapidaProjetosModal = $('#pesquisaRapidaProjetos');
		this.nomeInput = $('#descricaoModal');
		this.pesquisaRapidaBtn = $('.js-pesquisa-rapida-projetos-btn');
		this.pesquisaRapidaCancelarBtn = $('.js-pesquisa-rapida-projetos-cancelar-btn');
		this.containerTabelaPesquisa = $('#containerTabelaPesquisaRapidaProjetos');
		this.htmlTabelaPesquisa = $('#tabela-pesquisa-rapida-projeto').html();
		this.template = Handlebars.compile(this.htmlTabelaPesquisa);
		this.mensagemErro = $('.js-mensagem-erro-projeto');
		this.closePesquisaRapidaBtn = $('.js-close-pesquisa-rapida');
	}
	
	PesquisaRapidaProjeto.prototype.iniciar = function() {
		this.closePesquisaRapidaBtn.on('click', onPesquisaRapidaCancelarClicado.bind(this));
		this.pesquisaRapidaCancelarBtn.on('click', onPesquisaRapidaCancelarClicado.bind(this));
		this.pesquisaRapidaProjetosModal.on('shown.bs.modal', onModalShow.bind(this));
	}
	
	function onModalShow() {
		onPesquisaRapidaClicado.call(this);
	}
	
	function onPesquisaRapidaClicado(event) {
		$.ajax({
			url: '/projetos',
			method: 'GET',
			contentType: 'application/json',
			success: onPesquisaConcluida.bind(this),
			error: onErroPesquisa.bind(this)
		});
	}
	
	function onPesquisaRapidaCancelarClicado(event) {
		event.preventDefault();
		this.pesquisaRapidaProjetosModal.modal('hide');
	}
	
	function onPesquisaConcluida(resultado) {
		var html = this.template(resultado);
		this.containerTabelaPesquisa.html(html);
		this.mensagemErro.addClass('hidden');
		
		var tabelaProjetoPesquisaRapida = new Yapont.TabelaProjetoPesquisaRapida(this.pesquisaRapidaProjetosModal);
		tabelaProjetoPesquisaRapida.iniciar();
	}
	
	function onErroPesquisa() {
		this.mensagemErro.removeClass('hidden');
	}
	
	return PesquisaRapidaProjeto;
	
}());

Yapont.TabelaProjetoPesquisaRapida = (function() {
	
	function TabelaProjetoPesquisaRapida(modal) {
		this.modalProjeto = modal;
		this.projeto = $('.js-projeto-pesquisa-rapida');
	}
	
	TabelaProjetoPesquisaRapida.prototype.iniciar = function() {
		this.projeto.on('click', onProjetoSelecionado.bind(this));
	}
	
	function onProjetoSelecionado(evento) {
		this.modalProjeto.modal('hide');
		
		var projetoSelecionado = $(evento.currentTarget);
		$('#codigoProjeto').val(projetoSelecionado.data('codigo'));
		$('#descricaoProjeto').val(projetoSelecionado.data('descricao'));
		
		var apontamento = new Yapont.Apontamento(projetoSelecionado.data('codigo'));
		apontamento.iniciar();
	}
	
	return TabelaProjetoPesquisaRapida;
	
}());

$(function() {
	var pesquisaRapidaProjeto = new Yapont.PesquisaRapidaProjeto();
	pesquisaRapidaProjeto.iniciar();
});