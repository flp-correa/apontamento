Yapont.TabelaUsuarios = (function () {
	
	function TabelaUsuarios(autocomplete) {
		this.autocomplete = autocomplete;
		this.tabelaUsuariosContainer = $('.js-tabela-usuarios-container');
		this.uuid = $('#uuid').val();
	}
	
	TabelaUsuarios.prototype.iniciar = function() {
		this.autocomplete.on('usuario-selecionado', onUsuarioSelecionado.bind(this));
		$('.js-exclusao-usuario').on('click', onExclusaoUsuario.bind(this));
	}
	
	function onUsuarioSelecionado(evento, usuario) {
		var resposta = $.ajax({
			url: 'usuario',
			method: 'POST',
			data: {
				codigoUsuario: usuario.codigo,
				uuid: this.uuid
			}
		});
		
		resposta.done(onUsuarioAtualizadaNoServidor.bind(this));
	}
	
	function onUsuarioAtualizadaNoServidor(html) {
		this.tabelaUsuariosContainer.html(html);	
		$('.js-exclusao-usuario').on('click', onExclusaoUsuario.bind(this));
	}
	
	function onExclusaoUsuario(evento) {
		var codigoUsuario = $(evento.target).data('codigo-usuario');
		var resposta = $.ajax({
			url: 'usuario/' + this.uuid + '/' + codigoUsuario,
			method: 'DELETE'
		});
		
		resposta.done(onUsuarioAtualizadaNoServidor.bind(this));
	}
	
	return TabelaUsuarios;
	
}());

$(function() {		
	var autocomplete = new Yapont.Autocomplete($('.js-nome-usuario-input'), 'nome', 'usuario-selecionado');
	autocomplete.iniciar();
	
	var tabelaUsuarios = new Yapont.TabelaUsuarios(autocomplete);
	tabelaUsuarios.iniciar();
});