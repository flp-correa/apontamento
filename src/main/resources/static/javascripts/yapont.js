var Yapont = Yapont || {};

Yapont.MaskMoney = (function() {
	
	function MaskMoney() {
		this.decimal = $('.js-decimal');
		this.plain = $('.js-plain');
		this.number = $('.js-number');
	}
	
	MaskMoney.prototype.enable = function() {
//		this.decimal.maskMoney({ decimal: ',', thousands: '.' });
//		this.plain.maskMoney({ precision: 0, thousands: '.' });
		this.decimal.maskNumber({ decimal: ',', thousands: '.' });
		this.plain.maskNumber({ integer: true, thousands: '.' });
		this.number.maskNumber({ integer: true, thousands: '' });
	}
	
	return MaskMoney;
	
}());

Yapont.MaskPhoneNumber = (function() {
	
	function MaskPhoneNumber() {
		this.inputPhoneNumber = $('.js-phone-number');
	}
	
	MaskPhoneNumber.prototype.enable = function() {
		var maskBehavior = function (val) {
		  return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
		};
		
		var options = {
		  onKeyPress: function(val, e, field, options) {
		      field.mask(maskBehavior.apply({}, arguments), options);
		    }
		};
		
		this.inputPhoneNumber.mask(maskBehavior, options);
	}
	return MaskPhoneNumber;
	
}());

Yapont.BootstrapSwitch = (function() {
	
	function BootstrapSwitch() {
		this.situacaoCadastro = $('.js-situacao');
	}
	
	BootstrapSwitch.prototype.iniciar = function() {
		this.situacaoCadastro.bootstrapSwitch();
	}
	
	return BootstrapSwitch;
	
}());

Yapont.MascaraCpfCnpj = (function() {
	
	function MascaraCpfCnpj() {
		this.inputCnpj = $('.js-mascara-cnpj');
		this.inputCpf = $('.js-mascara-cpf');
	}
	
	MascaraCpfCnpj.prototype.iniciar = function() {
		this.inputCnpj.mask('00.000.000/0000-00');
		this.inputCpf.mask('000.000.000-00');
	}
	
	return MascaraCpfCnpj;
	
}());

Yapont.MaskCep = (function() {
	
	function MaskCep() {
		this.inputCep = $('.js-cep');
	}
	
	MaskCep.prototype.enable = function() {
		this.inputCep.mask('00.000-000');
	}
	
	return MaskCep;
	
}());

Yapont.MaskDate = (function() {
	
	function MaskDate() {
		this.inputDate = $('.js-date');
	}
	
	MaskDate.prototype.enable = function() {
		this.inputDate.mask('00/00/0000');
		this.inputDate.datepicker({
			orientation: 'bottom',
			language: 'pt-BR',
			todayHighlight: true,
			autoclose: true
		});
	}
	
	return MaskDate;
	
}());

Yapont.MaskHour = (function() {
	
	function MaskHour() {
		this.inputHour = $('.js-hour');
	}
	
	MaskHour.prototype.enable = function() {
		var mask = function (val) {
		    val = val.split(":");
		    return (parseInt(val[0]) > 19)? "HZ:M0" : "H0:M0";
		}

		pattern = {
		    onKeyPress: function(val, e, field, options) {
		        field.mask(mask.apply({}, arguments), options);
		    },
		    translation: {
		        'H': { pattern: /[0-2]/, optional: false },
		        'Z': { pattern: /[0-3]/, optional: false },
		        'M': { pattern: /[0-5]/, optional: false }
		    },
		    placeholder: 'hh:mm'
		};
		
		this.inputHour.mask(mask, pattern);
	}
	
	return MaskHour;
	
}());

Yapont.Security = (function() {
	
	function Security() {
		this.token = $('input[name=_csrf]').val();
		this.header = $('input[name=_csrf_header]').val();
	}
	
	Security.prototype.enable = function() {
		$(document).ajaxSend(function(event, jqxhr, settings) {
			jqxhr.setRequestHeader(this.header, this.token);
		}.bind(this));
	}
	
	return Security;
	
}());

numeral.language('pt-br');

Yapont.formatarMoeda = function(valor) {
	return numeral(valor).format('0,0.00');
}

Yapont.ValidarCpf = function(cpf) {
	cpf = cpf.replace(/[^0-9]/g, '').toString();
	
	// Elimina CNPJs invalidos conhecidos
    if (cpf == "00000000000" || 
        cpf == "11111111111" || 
        cpf == "22222222222" || 
        cpf == "33333333333" || 
        cpf == "44444444444" || 
        cpf == "55555555555" || 
        cpf == "66666666666" || 
        cpf == "77777777777" || 
        cpf == "88888888888" || 
        cpf == "99999999999") {
    	return false;
    }

    if( cpf.length == 11 ) {
        var v = [];

        //Calcula o primeiro dígito de verificação.
        v[0] = 1 * cpf[0] + 2 * cpf[1] + 3 * cpf[2];
        v[0] += 4 * cpf[3] + 5 * cpf[4] + 6 * cpf[5];
        v[0] += 7 * cpf[6] + 8 * cpf[7] + 9 * cpf[8];
        v[0] = v[0] % 11;
        v[0] = v[0] % 10;

        //Calcula o segundo dígito de verificação.
        v[1] = 1 * cpf[1] + 2 * cpf[2] + 3 * cpf[3];
        v[1] += 4 * cpf[4] + 5 * cpf[5] + 6 * cpf[6];
        v[1] += 7 * cpf[7] + 8 * cpf[8] + 9 * v[0];
        v[1] = v[1] % 11;
        v[1] = v[1] % 10;

        //Retorna Verdadeiro se os dígitos de verificação são os esperados.
        if ( (v[0] != cpf[9]) || (v[1] != cpf[10]) ) {
            return false;
        }
    } else {
        return false;
    }
    return true;
}

Yapont.ValidarCnpj = function(cnpj) {
	cnpj = cnpj.replace(/[^\d]+/g,'');
	 
	if(cnpj == '') {
		return false;
	}
	     
	if (cnpj.length != 14) {
		return false;
	}
	 
	// Elimina CNPJs invalidos conhecidos
	if (cnpj == "00000000000000" || 
		cnpj == "11111111111111" || 
	    cnpj == "22222222222222" || 
	    cnpj == "33333333333333" || 
	    cnpj == "44444444444444" || 
	    cnpj == "55555555555555" || 
	    cnpj == "66666666666666" || 
	    cnpj == "77777777777777" || 
	    cnpj == "88888888888888" || 
	    cnpj == "99999999999999") {
		return false;
	}
	         
	
	// Valida DVs
	tamanho = cnpj.length - 2
	numeros = cnpj.substring(0,tamanho);
	digitos = cnpj.substring(tamanho);
	soma = 0;
	pos = tamanho - 7;
	for (i = tamanho; i >= 1; i--) {
		soma += numeros.charAt(tamanho - i) * pos--;
		if (pos < 2) {
			pos = 9;
		}
	}
	
	resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
	if (resultado != digitos.charAt(0)) {
		return false;
	}
	    
	tamanho = tamanho + 1;
	numeros = cnpj.substring(0,tamanho);
	soma = 0;
	pos = tamanho - 7;
	for (i = tamanho; i >= 1; i--) {
		soma += numeros.charAt(tamanho - i) * pos--;
		if (pos < 2) {
			pos = 9;
		}
	}
	resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
	if (resultado != digitos.charAt(1)) {
		return false;
	}
	    
	return true;
}

Yapont.gerarUuid = function() {
	function s4() {
		return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
	}
	return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

Yapont.getRandomInt = function(min, max) {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min)) + min;
}

$(function() {
	var security = new Yapont.Security();
	security.enable();
	
	var maskMoney = new Yapont.MaskMoney();
	maskMoney.enable();
	
	var maskPhoneNumber = new Yapont.MaskPhoneNumber();
	maskPhoneNumber.enable();
	
	var situacaoCadastro = new Yapont.BootstrapSwitch();
	situacaoCadastro.iniciar();
	
	var mascaraCpfCnpj = new Yapont.MascaraCpfCnpj();
	mascaraCpfCnpj.iniciar();
	
	var maskCep = new Yapont.MaskCep();
	maskCep.enable();
	
	var maskDate = new Yapont.MaskDate();
	maskDate.enable();
	
	var maskHour = new Yapont.MaskHour();
	maskHour.enable();
	
//	$('input').attr('autocomplete','off');
});
